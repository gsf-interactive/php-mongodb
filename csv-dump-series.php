<?php
/**
 * Date: 8/7/17
 * Time: 3:51 PM
 * MAMP - scottfleming
 * Export a series from the products collection to csv for download.
 *
 */

if (!defined('PHP_VERSION_ID')) {
    $version = explode('.', PHP_VERSION);
    define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}

$mongo      = new MongoClient("mongodb://localhost");
$db         = $mongo->xville;
$products   = $db->products;

// All fields
$fields = array('wordpress_id','wordpress_title','product_type','description','series','color_name','color_number','sku','nominal_size','nominal_units','actual_size','actual_units','sheet_size','thickness','thickness_units','grout_joint','calibrated_rectified','finish','tile_type','trim_type','recycled_content','green_squared_certified','made_in_usa','hydrotect_available','shade_variation','dcof','get_planked_available','body_type','color_reference','style','exterior_paving','interior_floors_dry','interior_floors_wet_areas','shower_floor_linear_drains','exterior_walls','exterior_covered_walls','interior_walls_dry','interior_walls_wet','counters','pool_fountain_full_lining','waterline_pool_fountain','tile_over_tile','special_notes','associated_img_url','featured_image_url','header_img_url','header_video_url','header_video_thumbnail','sizesContent','sample_sizes','custom_size_image','trim_image');



if( isset($_POST['series']) ){

    if(empty($_POST['series']) && empty($_POST['all'])){
        header('location: csv-dump-series.php');
    }

    $series = $_POST['series'];
    $all    = $_POST['all'];
    if($all){
        // get entire collection
        $series     = 'xville.products';
        $allitems   = iterator_to_array($products->find());

    }elseif($series){
        // find a single series
        $finder = array(
            '$and'  => array(
                array(
                    "series"            => new MongoRegex('/'.$series. '/i')
                )
            )
        );
        $allitems   = iterator_to_array($products->find($finder));
    }// if series


        $series     = str_replace(' ','_', $series) . '-' . date('m').date('d').date('y');

        $count      = count($allitems);
        $fp         = fopen('php://output', 'w');


        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="'.$series .'.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');

        fputcsv($fp, $fields);

        foreach($allitems as $key => $data){

            array_shift($data);
            fputcsv($fp, $data);
        }


}else{


?>
    <html>
    <title>Export Series to CSV</title>
    <link rel="stylesheet" href="css/csvdump.css"/>
    <body>
    <h1>Crossville Products Exporter to CSV</h1>
    <p>Real easy. Choose a series and click fire! I'll spew you a csv file for your review and easy edits.</p>
    <div class="form_container">
    <form name="series" method="POST" action="csv-dump-series.php">
       <div class="styled">
        <select name="series">
        <option value="">Choose A Series</option>
        <?php
        $fields = array("series");
        $allitems   = iterator_to_array($products->find());
        $options    = array();

        foreach($allitems as $key => $data){
            foreach($fields as $field){
                if(! in_array($data[$field],$options)){
                    array_push($options, $data[$field]);
                }

            }
        }

        sort($options);

        foreach($options as $option){
        echo "<option value='".$option."'>".$option."</option>";
        }
        ?>
        </select>
       </div>
        <input type="checkbox" name="all" id="all"/>
        <label for="all">Just gimme everything ya got Scotty!<br/>
        The whole heap of it!
        </label>

        <br clear="all"/>
        <input type="submit" value="Fire!" />
    </form>
    </div>
    </body>
    </html>


<?php
} // if $_POST['series']
?>
