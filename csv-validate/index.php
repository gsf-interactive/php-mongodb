<?php

// Rules of the CSV

?>

<style>
    html{font-family: arial;}
    .filestuff{
        margin: 4px;
    }
    .error{color: red; padding: 4px;}
    .ok {white-space:nowrap;}
    table, th, td {
        border-collapse: collapse;
        border: 1px solid black;
        padding: 2px;
    }th {
     text-transform: uppercase;
    }

    .formtable table{
        border: none;

    }
    .formtable td{
        border: none; padding: 20px;
    }

</style>

<table width="600" class="formtable">
    <form method="post" enctype="multipart/form-data">
        <tr>
            <td width="20%">Select file</td>
            <td width="80%"><input type="file" name="file" id="file" /></td>
        </tr>
        <tr>
            <td>Submit</td>
            <td><input type="submit" name="submit" /></td>
        </tr>
    </form>
</table>
<?php

if ( isset($_POST["submit"]) ) {



    $errs = array(
        0 => 'Upload OK',
        1 => 'The uploaded file exceeds the upload_max_filesize directive. Use a smaller file.',
        2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
        3 => 'The uploaded file was only partially uploaded.'
    );



    if ( isset($_FILES["file"])) {

        if($_FILES['file']['type'] != 'text/csv'){
        die('<div><br/>OH NO! Must be a CSV file!!!</div>');

        }
        //if there was an error uploading the file
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
            echo $errs[$_FILES["file"]["error"]];
        }
        else {
            //Print file details
            echo "<div class='filestuff'>";
            echo "Upload: " . $_FILES["file"]["name"] . "<br />";
            echo "Type: " . $_FILES["file"]["type"] . "<br />";
            echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
            echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";

            //Store file in directory "upload" with the name of "uploaded_file.txt"
            $storagename = $_FILES["file"]["name"];
            move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $storagename);
            echo "Stored in: " . "upload/" . $_FILES["file"]["name"] . "<br />";
            echo "</div>";

        }
    } else {
        echo "No file selected <br />";
    }
    $err        = 0;
    $csv        = array_map("str_getcsv", file("upload/".$storagename, FILE_SKIP_EMPTY_LINES));
    $header     = array_shift($csv);
    $col_count  = count($header);
    $legal_cols = 52;

    echo "<div>";
    echo "There are $col_count columns in this file. ";
    echo $legal_cols == $col_count ? " Correct Column count. " : " Columns DONT add up! Check your CSV file and try again!";
    echo "</div><br/>";

    if($legal_cols != $col_count):
        die();
    endif;


    foreach ($csv as $i => $row) {
        $csv[$i] = array_combine($header, $row);
    }


    echo "<table><tr>";
    foreach($header as $headerCol){
        echo "<th>" .$headerCol . "</th>";
    }
    echo "</tr>";


    $rules = array("wordpress_id" => 'n');


    foreach($csv as $row){
        echo "<tr>";
        foreach($row as $key => $value){
            $class="ok";

           switch($key){

               case 'wordpress_id':
                   if(!is_numeric($value)){
                       $value   = "NaN!<br/>$value";
                        $class  = "error";
                       $err++;
                   }
               break;

               case 'nominal_units':
                   $find_letters = array('M', 'inch', 'meter');
                   if(strpbrk($value, implode($find_letters)) === false)                   {
                       $value = "Illegal value!<br/>$value";
                       $class="error";
                       $err++;
                   }
               break;

               case 'thickness_units':
                   $find_letters = array('mm', 'inch');
                   if(strpbrk($value, implode($find_letters)) === false)                   {
                       $value = "Illegal value!<br/>$value";
                       $class="error";
                       $err++;
                   }
               break;

               case ('color_name') :
               case ('wordpress_title'):
                   if(strlen($value) < 3){
                       $value  ="Length error!<br/> $value";
                       $class="error";
                       $err++;
                   }
                break;
               case ('sku'):
               case ('color_number'):
               if(strlen($value) < 3){
                   $value  = "Length error!<br/>$value";
                   $class="error";
                   $err++;
               }
               break;

               case 'actual_size':
               case 'nominal_size':
                   if(! preg_match('/ X /i', $value)){
                       $value = "Bad value!<br/>$value";
                       $class="error";
                       $err++;
                   }
               break;

               case 'associated_img_url':
               case 'featured_image_url':
               case 'header_img_url':

               $title   = $value;
               $value = substr($value, 0, 30) . '...';

               break;
           }

            echo "<td class='$class' title='$title'>". $value . "</td>";



        }
        echo "</tr>";
    }
    echo "</table>";

    if($err > 0):
        echo "<div class='error'>You have $err errors in this file! Please fix, and re-test by uploading again!</div>";
    else:
        echo "<br/><div>Your file is clean, and I see no errors. Good job!</div>";
    endif;

}


?>

