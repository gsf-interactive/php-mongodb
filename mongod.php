<html>
<title>Scott's Little MongoDB Show</title>
<head>
   <link href="css/csvdump.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<?php
/**
 * Date: 8/7/17
 * Time: 3:51 PM
 * MAMP - scottfleming
 * A demonstration in MongoDB in PHP with the products api application.
 *
 */

if (!defined('PHP_VERSION_ID')) {
    $version = explode('.', PHP_VERSION);
    define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}

// All fields
$fields = array('wordpress_id','wordpress_title','product_type','description','series','color_name','color_number','sku','nominal_size','nominal_units','actual_size','actual_units','sheet_size','thickness','thickness_units','grout_joint','calibrated_rectified','finish','tile_type','trim_type','recycled_content','green_squared_certified','made_in_usa','hydrotect_available','shade_variation','dcof','get_planked_available','body_type','color_reference','style','exterior_paving','interior_floors_dry','interior_floors_wet_areas','shower_floor_linear_drains','exterior_walls','exterior_covered_walls','interior_walls_dry','interior_walls_wet','counters','pool_fountain_full_lining','waterline_pool_fountain','tile_over_tile','special_notes','associated_img_url','featured_image_url','header_img_url','header_video_url','header_video_thumbnail','sizesContent','sample_sizes','custom_size_image','trim_image');

#$fields = array('color_number','color_name','exterior_paving','interior_floors_dry','interior_floors_wet_areas','shower_floor_linear_drains','exterior_walls','exterior_covered_walls','interior_walls_dry','interior_walls_wet','counters','pool_fountain_full_lining','waterline_pool_fountain','tile_over_tile');

// short version
#$fields = array('wordpress_title','series' , 'product_type','sku','thickness','thickness_units','nominal_size','nominal_units','actual_size','actual_units','sheet_size','thickness','thickness_units');


$fields = array(
    'series',
    'color_name',
    'color_number',
    'sku',
    'featured_image_url',
    'custom_size_image',
    'trim_image'
);


echo "<h3>PHP Version " . PHP_VERSION . " with MongoClient extension for MAMP</h3>";

// Show PHP errors (during development only)
    error_reporting(E_ALL | E_STRICT);
    ini_set("display_errors", 2);

    // Create a Mongo conenction
    $mongo = new MongoClient("mongodb://localhost");

    // Choose our database and collection
    $db         = $mongo->xville;
    $products   = $db->products;
    $finder     = [];
    $filters    = [];

    // get distinct values of a field -- useful for building filters

    foreach( $fields as $field){
            $filters[$field] = array_filter( $products->distinct($field) ); // removes empty records
            sort($filters[$field]); // sorts the array
    }

    // print_r($filters);


    // Sample update of data with query for wordpress_id
    // Useful way to update a single field in a match

    // Update products set special_notes = "Basketweave Mosaic"
    /// WHERE series like 'Bohemia' AND nominal_size = '12 x 12'
    // on multiple rows.

/*
    $newdata =  array(
                            "special_notes"    => "Basketweave Mosaic"
    );
    $products->update( array('$and' => array(
                                        array(
                                                    "series" => new MongoRegex ("/Bohemia/i")
                                            ),
                                            array(
                                                    "nominal_size"  => new MongoRegex("/12 x 12/")
                                                )
                                           )),
                        array('$set' => $newdata),
                        array("multiple" => true)
                    );
*/

    #
    echo "<pre>";

    //
    // Delete a series from the entire collection with a single command
    //

     #$products->remove(array("series" => new MongoRegex('/notorious/i')));

    // Build a query to set which fields and matches, including
    // A SEMI-like feature using MongoRegex and filter by title

    $finder = array(
            '$or'  => array(

                array(
                    "series"            => new MongoRegex('/nest/i')
                ),

            )
    );

    $allitems   = iterator_to_array($products->find($finder));
    $count      = count($allitems);
    $colors     = $fiu = $skus = array();

    print_r($finder);


    // Let's count colors in the series
    foreach($allitems as $key => $color) {
        array_push($colors, $color['color_name']);
        array_push($fiu, $color['featured_image_url']);
        array_push($skus, $color['sku']);
    }

$arr        = array_unique($skus);

$duplicates = array_diff($arr, $skus);

print_r( array_unique($colors));


echo "</pre>";
    echo "<h3>" . count(array_unique( $colors) ) . " unique colors in the series.</h3>";
    echo "<h3>" . count(array_unique( $fiu) ) . " unique featured images of ". count($fiu). " in the series.</h3>";
    echo "<h3>" . count($arr) . " unique skus  of ". count($skus) . " in the series. </h3>";


    $header_keys = "<table><tr><th>" . implode("</th><th>", $fields) . "</th></tr>";
    echo $header_keys . "\n";

    $url_list       = array();

    foreach($allitems as $key => $data){

        echo "<tr valign=middle>";

        foreach ($fields as $field){
            if(in_array( $field, array('featured_image_url','custom_size_image','header_img_url','trim_image'))){
                $urls = $data[$field];
                $pathinfo = pathinfo($urls);

                echo !empty($urls)? "<td><img valign=middle height='50' src='".$urls."'> ". $pathinfo['basename'] ." </td>" : "<td></td>";
            }else{
                echo "<td>". mb_convert_encoding($data[$field], 'UTF-8', 'UTF-8') . "</td>";
            }
        }

        echo "</tr>";

}

echo "</table>";

?>

</html>
